FROM httpd:2.4
ENV GITLAB_OMNIBUS_CONFIG="external_url 'http://localhost'; gitlab_rails['pipeline_schedule_worker_cron'] = true;"
COPY ./index.html /usr/local/apache2/htdocs/